### Project Description
CRUD Rest API for an user with propagating events on success (microservices architecture)
 * Requirements : Docker
 * Techno : Maven / Java 8 / Spring boot / Apache kafka / Postgres / Swagger
### This repository contains below project
* commons : commons library used by services (annotation, constant, ...)
* discovery-server : Eureka discovery server
* gateway-server : Zuul Gateway server
* service-notification : service to implement notification (email , ..)
* service-user : CRUD actions for user 

### Launch project
go to the root of the project ``/ricardo`` and launch:
* docker-compose build (to build artefact)
* docker-compose up -d (to start)
* docker-compose down (to stop)

### Access
* Eureka server available on http://localhost:8761
* Gateway server available on http://localhost:8080 (no IHM)
* Swagger based documentation => http://localhost:8080/swagger-ui.html (refresh if docs is not availabe)

** Use either postman or swagger to test app **