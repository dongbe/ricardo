package com.ricardo.userservice.config;

import com.ricardo.common.event.EventProcessor;
import com.ricardo.common.event.NotificationAspect;
import com.ricardo.common.event.NotificationDto;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;

@EnableKafka
@Configuration
public class KafkaConfiguration {

    private KafkaProperties kafkaProperties;
    private EventProcessor eventProcessor;

    public KafkaConfiguration(KafkaProperties kafkaProperties, EventProcessor eventProcessor) {
        this.eventProcessor = eventProcessor;
        this.kafkaProperties = kafkaProperties;
    }

    @Bean
    public ProducerFactory<String, NotificationDto> producerFactory() {
        return new DefaultKafkaProducerFactory<>(this.kafkaProperties.getProducerProps());
    }

    @Bean
    public KafkaTemplate<String, NotificationDto> kafkaTemplate() {
        return new KafkaTemplate<>(producerFactory());
    }

    @Bean
    public NotificationAspect getNotificationAspect() {
        return new NotificationAspect("intra_events", kafkaTemplate());
    }

}
