package com.ricardo.userservice.exception;

public class WrongCountryException extends RuntimeException {

    public WrongCountryException(String message) {
        super(message);
    }

}
