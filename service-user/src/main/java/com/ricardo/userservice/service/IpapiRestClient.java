package com.ricardo.userservice.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.concurrent.CompletableFuture;

@Slf4j
@Service
public class IpapiRestClient {

    @Autowired
    private RestTemplate restTemplate;

    @Async("asyncExecutor")
    public CompletableFuture<Boolean> isFromCountry(String countryCode, String ipAddress) {
        log.info("isFrom country {} starts {}", countryCode, ipAddress);
        String url = "https://ipapi.co/" + ipAddress + "/country/";

        try {
            String actualCountry = restTemplate.getForObject(
                    url, String.class);
            log.info("isFrom country {} completed, {}", countryCode, actualCountry);
            return CompletableFuture.completedFuture(countryCode.equalsIgnoreCase(actualCountry));
        } catch (Exception ex) {
            return CompletableFuture.completedFuture(false);
        }
    }


}
