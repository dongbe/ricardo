package com.ricardo.userservice.service;

import com.ricardo.common.annotation.Notification;
import com.ricardo.common.enums.NOTIF_TYPE;
import com.ricardo.userservice.dto.UserDto;
import com.ricardo.userservice.dto.UserFullDto;
import com.ricardo.userservice.exception.UserNotFoundException;
import com.ricardo.userservice.mapper.UserFullMapper;
import com.ricardo.userservice.mapper.UserMapper;
import com.ricardo.userservice.model.User;
import com.ricardo.userservice.repository.UserRepository;
import com.ricardo.userservice.repository.UserSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
public class UserServiceImpl implements UserServiceInt {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private UserFullMapper userFullMapper;

    @Autowired
    private UserRepository userRepository;

    @Notification(template = "create.user")
    @Override
    public UUID createUser(UserDto dto) {
        User user = userMapper.mapTo(dto);
        user = userRepository.save(user);
        return user.getId();
    }

    @Notification(template = "update.user", type = NOTIF_TYPE.UPDATE)
    @Override
    public UUID updateUser(UUID id, UserDto dto) {
        Optional<User> oldUser = userRepository.findById(id);
        if (!oldUser.isPresent())
            throw new UserNotFoundException("User not found exception");
        User user = userMapper.mapTo(dto, oldUser.get());
        userRepository.save(user);
        return user.getId();
    }

    @Override
    public UserDto getUser(UUID id) {
        Optional<User> optionalUser = userRepository.findById(id);
        if (!optionalUser.isPresent())
            throw new UserNotFoundException("User not found exception");
        return userMapper.mapFrom(optionalUser.get());
    }

    @Override
    public Page<UserFullDto> getUsers(String keyword, Pageable pageable) {
        Page<User> users = keyword != null
                ? userRepository.findAll(UserSpecification.searchUser(keyword), pageable)
                : userRepository.findAll(pageable);
        return users.map(user -> userFullMapper.mapFrom(user));
    }

    @Override
    public void deleteUser(UUID id) {
        userRepository.deleteById(id);
    }
}
