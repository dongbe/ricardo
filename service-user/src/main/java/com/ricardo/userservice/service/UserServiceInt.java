package com.ricardo.userservice.service;


import com.ricardo.userservice.dto.UserDto;
import com.ricardo.userservice.dto.UserFullDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.UUID;

public interface UserServiceInt {

    UUID createUser(UserDto dto);

    UUID updateUser(UUID id, UserDto dto);

    UserDto getUser(UUID id);

    Page<UserFullDto> getUsers(String keyword, Pageable pageable);

    void deleteUser(UUID id);
}
