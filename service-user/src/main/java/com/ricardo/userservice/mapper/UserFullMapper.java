package com.ricardo.userservice.mapper;

import com.ricardo.common.mapper.AbstractEntityMapper;
import com.ricardo.common.mapper.LocalDateMapper;
import com.ricardo.userservice.dto.UserFullDto;
import com.ricardo.userservice.model.User;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {LocalDateMapper.class})
public abstract class UserFullMapper extends AbstractEntityMapper<UserFullDto, User> {
}
