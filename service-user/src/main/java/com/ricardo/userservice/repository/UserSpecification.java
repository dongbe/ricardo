package com.ricardo.userservice.repository;

import com.ricardo.common.constant.Constants;
import com.ricardo.common.constant.Patterns;
import com.ricardo.userservice.model.User;
import org.apache.commons.lang.StringUtils;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class UserSpecification {

    public static Specification<User> searchUser(String keyword) {

        return (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            if (StringUtils.isNotBlank(keyword)) {
                final String searchKey = Constants.SQL_LIKE_SYMBOL + keyword.toUpperCase() + Constants.SQL_LIKE_SYMBOL;
                predicates.add(criteriaBuilder.like(criteriaBuilder.upper(root.get("name")), searchKey));
                predicates.add(criteriaBuilder.like(criteriaBuilder.upper(root.get("firstName")), searchKey));
                predicates.add(criteriaBuilder.like(criteriaBuilder.upper(root.get("email")), searchKey));
                predicates.add(criteriaBuilder.like(criteriaBuilder.upper(root.get("address")), searchKey));
                predicates.add(criteriaBuilder.like(criteriaBuilder.function("TO_CHAR", String.class, root.<LocalDate>get("birthDate"), criteriaBuilder.literal(Patterns.DATE_PATTERN_FR_SHORT)), searchKey));
            }
            return criteriaBuilder.or(predicates.toArray(new Predicate[predicates.size()]));
        };
    }
}
