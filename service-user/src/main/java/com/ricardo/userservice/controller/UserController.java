package com.ricardo.userservice.controller;

import com.ricardo.common.enums.COUNTRY_CODE;
import com.ricardo.common.filter.BasicFilter;
import com.ricardo.userservice.dto.UserDto;
import com.ricardo.userservice.dto.UserFullDto;
import com.ricardo.userservice.exception.WrongCountryException;
import com.ricardo.userservice.service.IpapiRestClient;
import com.ricardo.userservice.service.UserServiceInt;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import static com.ricardo.common.constant.Constants.HEADER_X_TOTAL_COUNT;


@RestController
public class UserController {

    private UserServiceInt service;

    @Autowired
    private IpapiRestClient ipapiRestClient;

    public UserController(UserServiceInt service) {
        this.service = service;
    }

    @PostMapping("/usernocheck")
    public UUID createUserNoCheck(@Valid @RequestBody UserDto dto) {
        return service.createUser(dto);
    }

    @PostMapping("/user")
    public UUID createUser(@Valid @RequestBody UserDto dto, HttpServletRequest request) throws ExecutionException, InterruptedException {
        CompletableFuture<Boolean> optionalPass = ipapiRestClient.isFromCountry(COUNTRY_CODE.CH.name(), request.getRemoteAddr());
        CompletableFuture.allOf(optionalPass).join();
        if (!optionalPass.get()) {
            throw new WrongCountryException("Action not allowed from your country");
        }
        return service.createUser(dto);
    }

    @PutMapping("/user/{id}")
    public void updateUser(@PathVariable UUID id, @Valid @RequestBody UserDto dto) {
        service.updateUser(id, dto);
    }

    @GetMapping("/user/{id}")
    public UserDto getUser(@PathVariable UUID id) {
        return service.getUser(id);
    }

    @DeleteMapping("/user/{id}")
    public void deleteUser(@PathVariable UUID id) {
        service.deleteUser(id);
    }

    @PostMapping("/users")
    public ResponseEntity<List<UserFullDto>> getAllUsers(@RequestBody(required = false) BasicFilter filter, Pageable pageable) {
        Page<UserFullDto> page = service.getUsers(filter != null && StringUtils.isNotBlank(filter.getKeyword())? filter.getKeyword() : null, pageable);
        HttpHeaders headers = new HttpHeaders();
        headers.add(HEADER_X_TOTAL_COUNT, Long.toString(page.getTotalElements()));
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }
}
