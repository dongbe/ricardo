package com.ricardo.userservice.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
public class UserFullDto extends UserDto {

    private UUID id;
}
