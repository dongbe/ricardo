package com.ricardo.userservice.dto;

import com.ricardo.common.constant.Patterns;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Getter
@Setter
public class UserDto {

    @NotBlank(message = "Le nom est obligatoire")
    @Size(max = 50)
    private String name;

    @NotBlank(message = "Le prénom est obligatoire")
    @Size(max = 50)
    private String firstName;

    @NotBlank(message = "La date de naissance est obligatoire")
    @Pattern(regexp = Patterns.DATE_PATTERN_FR)
    private String birthDate;

    @Email
    @Size(min = 5, max = 254)
    private String email;

    @Size(max = 256)
    private String address;
}
