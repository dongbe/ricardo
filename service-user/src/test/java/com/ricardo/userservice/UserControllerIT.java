package com.ricardo.userservice;

import com.ricardo.common.utils.DateUtils;
import com.ricardo.userservice.controller.UserController;
import com.ricardo.userservice.dto.UserDto;
import com.ricardo.userservice.exception.GlobalExceptionHandler;
import com.ricardo.userservice.mapper.UserMapper;
import com.ricardo.userservice.model.User;
import com.ricardo.userservice.repository.UserRepository;
import com.ricardo.userservice.service.UserServiceInt;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Integration tests for the {@link com.ricardo.userservice.controller.UserController} REST controller.
 */
@SpringBootTest(classes = {UserServiceApplication.class})
public class UserControllerIT {

    private static final String DEFAULT_FIRSTNAME = "john";

    private static final String DEFAULT_LASTNAME = "doe";

    private static final String DEFAULT_BDAY = "01/01/1990";

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private UserServiceInt userServiceInt;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private GlobalExceptionHandler exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restUserMockMvc;

    private User user;

    /**
     * Create a User.
     * <p>
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which has a required relationship to the User entity.
     */
    public static User createEntity(EntityManager em) {
        User user = new User();
        user.setFirstName(DEFAULT_FIRSTNAME);
        user.setBirthDate(DateUtils.convertStringToDate(DEFAULT_BDAY, "dd/MM/yyyy"));
        user.setName(DEFAULT_LASTNAME);
        return user;
    }

    @BeforeEach
    public void setup() {
        UserController userResource = new UserController(userServiceInt);

        this.restUserMockMvc = MockMvcBuilders.standaloneSetup(userResource)
                .setCustomArgumentResolvers(pageableArgumentResolver)
                .setControllerAdvice(exceptionTranslator)
                .setMessageConverters(jacksonMessageConverter)
                .build();
    }

    @BeforeEach
    public void initTest() {
        user = createEntity(em);
    }

    @Test
    @Transactional
    public void getAllUsers() throws Exception {
        // Initialize the database
        userRepository.saveAndFlush(user);

        // Get all the users
        restUserMockMvc.perform(post("/users?sort=id,desc")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.[*].firstName").value(hasItem(DEFAULT_FIRSTNAME)))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_LASTNAME)));
    }

    @Test
    @Transactional
    public void getUser() throws Exception {
        // Initialize the database
        userRepository.saveAndFlush(user);
        // Get the user
        restUserMockMvc.perform(get("/user/{id}", user.getId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.firstName").value(DEFAULT_FIRSTNAME))
                .andExpect(jsonPath("$.name").value(DEFAULT_LASTNAME));
    }

    @Test
    @Transactional
    public void getNonExistingUser() throws Exception {
        restUserMockMvc.perform(get("/user/23fae6c6-48a2-4dec-a377-97958dc5189c"))
                .andExpect(status().isNotFound());
    }


    @Test
    public void testUserDTOtoUser() {
        UserDto userDTO = new UserDto();
        userDTO.setFirstName(DEFAULT_FIRSTNAME);
        userDTO.setName(DEFAULT_LASTNAME);

        User user = userMapper.mapTo(userDTO);
        assertThat(user.getFirstName()).isEqualTo(DEFAULT_FIRSTNAME);
        assertThat(user.getName()).isEqualTo(DEFAULT_LASTNAME);
    }

    @Test
    public void testUserToUserDTO() {
        UserDto userDTO = userMapper.mapFrom(user);
        assertThat(userDTO.getFirstName()).isEqualTo(DEFAULT_FIRSTNAME);
        assertThat(userDTO.getName()).isEqualTo(DEFAULT_LASTNAME);

    }
}
