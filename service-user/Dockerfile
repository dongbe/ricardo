FROM maven:3-jdk-8-alpine as common
WORKDIR /app
COPY pom.xml .
COPY common/pom.xml ./common/pom.xml
WORKDIR /app/common
RUN mvn dependency:go-offline
COPY common/src/ ./src/
RUN mvn package

FROM maven:3-jdk-8-alpine as build
WORKDIR /app
COPY --from=common /root/.m2 /root/.m2
COPY --from=common /app/common/target/common-1.0-SNAPSHOT.jar /app/common-1.0-SNAPSHOT.jar
RUN mvn install:install-file \
   -Dfile=common-1.0-SNAPSHOT.jar \
   -DgroupId=com.ricardo \
   -DartifactId=common \
   -Dversion=1.0-SNAPSHOT \
   -Dpackaging=jar \
   -DgeneratePom=true
COPY service-user/pom.xml ./service-user/pom.xml
WORKDIR /app/service-user
RUN mvn dependency:go-offline
COPY service-user/src/ ./src/
COPY service-user/entrypoint.sh ./entrypoint.sh
RUN mvn package

FROM openjdk:8-jre-alpine
# Add a jhipster user to run our application so that it doesn't need to run as root
RUN adduser -D -s /bin/sh user
WORKDIR /app
COPY --from=build /app/service-user/target/service-user-0.0.1-SNAPSHOT.jar service-user.jar
COPY --from=build /app/service-user/entrypoint.sh entrypoint.sh
RUN chmod 755 entrypoint.sh && chown user:user entrypoint.sh
USER user

ENV PORT 8082
EXPOSE $PORT

ENTRYPOINT ["./entrypoint.sh"]
