package com.ricardo.notificationservice.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class NotificationController {

    @PostMapping("/email")
    public ResponseEntity<String> sendMail() {
        return new ResponseEntity<>("Not implemented", HttpStatus.OK);
    }

    @PostMapping("/sms")
    public ResponseEntity<String> sendSms() {
        return new ResponseEntity<>("Not implemented", HttpStatus.OK);
    }
}
