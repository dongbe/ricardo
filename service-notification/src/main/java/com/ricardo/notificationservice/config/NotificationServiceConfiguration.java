package com.ricardo.notificationservice.config;

import com.ricardo.common.event.EventProcessor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.client.RestTemplate;

@Configuration
@EnableAspectJAutoProxy
@EnableAsync
public class NotificationServiceConfiguration {

    private KafkaProperties kafkaProperties;
    private EventProcessor eventProcessor;

    public NotificationServiceConfiguration(KafkaProperties kafkaProperties, EventProcessor eventProcessor) {
        this.eventProcessor = eventProcessor;
        this.kafkaProperties = kafkaProperties;
    }

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
}
