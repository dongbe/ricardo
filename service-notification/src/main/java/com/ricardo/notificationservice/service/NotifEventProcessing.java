package com.ricardo.notificationservice.service;

import com.ricardo.common.event.EventProcessor;
import com.ricardo.common.event.NotificationDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class NotifEventProcessing implements EventProcessor {
    @Override
    public void process(NotificationDto dto) {
        log.info("Event received, {} - {}", dto.getTemplate(), dto.getType().name());
    }
}
