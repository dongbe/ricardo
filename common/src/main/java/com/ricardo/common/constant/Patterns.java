package com.ricardo.common.constant;

/**
 * Application patterns
 */
public final class Patterns {

    public static final String DATE_PATTERN_FR = "[0-3][0-9]/[0-1][0-9]/[0-9]{4}";

    public static final String DATE_PATTERN_US = "MM/dd/yyyy";

    public static final String DATE_PATTERN_FR_SHORT = "dd/MM/yy";

    public static final String DATE_PATTERN_FR_FULL = "dd/MM/yyyy";
}
