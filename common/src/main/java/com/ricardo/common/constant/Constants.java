package com.ricardo.common.constant;

public class Constants {
    public static final String HEADER_X_TOTAL_COUNT = "X-Page-Count";
    public static final String SQL_LIKE_SYMBOL = "%";
}
