package com.ricardo.common.event;

import com.ricardo.common.annotation.Notification;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.kafka.core.KafkaTemplate;

@Aspect
@Slf4j
public class NotificationAspect {

    private KafkaTemplate<String, NotificationDto> kakfaProducer;
    private String topic;

    public NotificationAspect(String topic, KafkaTemplate<String, NotificationDto> kakfaProducer) {
        this.topic = topic;
        this.kakfaProducer = kakfaProducer;
    }

    @After("execution(* *.*(..)) && @annotation(notification)")
    public void sendNotif(JoinPoint joinPoint, Notification notification) {
        NotificationDto dto = new NotificationDto();
        dto.setTemplate(notification.template());
        dto.setType(notification.type());
        this.kakfaProducer.send(topic, dto);
        log.info("Event produced, {} - {}", dto.getTemplate(), dto.getType().name());
    }

}
