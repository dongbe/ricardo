package com.ricardo.common.event;

import com.ricardo.common.enums.NOTIF_TYPE;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NotificationDto {
    private NOTIF_TYPE type;
    private String template;
}
