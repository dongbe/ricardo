package com.ricardo.common.event;

public interface EventProcessor {

    void process(NotificationDto dto);
}
