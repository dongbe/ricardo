package com.ricardo.common.event;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;

@Slf4j
@AllArgsConstructor
public class IntraEventConsumer {

    private EventProcessor eventProcessor;

    @KafkaListener(topics = {"intra_events"}, autoStartup = "true")
    public void listenToProjectChange(ConsumerRecord<String, NotificationDto> record) {
        this.eventProcessor.process(record.value());
    }
}