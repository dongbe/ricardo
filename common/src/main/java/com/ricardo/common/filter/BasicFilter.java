package com.ricardo.common.filter;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BasicFilter {

    private String keyword;
}
