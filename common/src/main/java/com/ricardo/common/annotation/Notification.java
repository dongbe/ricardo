package com.ricardo.common.annotation;

import com.ricardo.common.enums.NOTIF_TYPE;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface Notification {
    NOTIF_TYPE type() default NOTIF_TYPE.CREATE;

    String template() default "";
}
