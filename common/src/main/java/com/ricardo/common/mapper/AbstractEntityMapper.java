package com.ricardo.common.mapper;

import java.lang.reflect.Field;

public abstract class AbstractEntityMapper<D, E> implements EntityMapper<D, E> {

    public E mapTo(D dto, E entity) {
        E obj = this.mapTo(dto);
        try {
            return merge(entity, obj);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public D mapFrom(E entity, D dto) {
        return dto;
    }

    public <T> T merge(T fromDb, T fromDto) throws IllegalAccessException, InstantiationException {
        Class<?> clazz = fromDb.getClass();
        Object merged = clazz.newInstance();
        for (Field field : clazz.getDeclaredFields()) {
            field.setAccessible(true);
            Object localValue = field.get(fromDb);
            Object remoteValue = field.get(fromDto);
            field.set(merged, (remoteValue != null) ? remoteValue : localValue);
        }
        if (clazz.getSuperclass() != null) {
            for (Field field : clazz.getSuperclass().getDeclaredFields()) {
                field.setAccessible(true);
                Object localValue = field.get(fromDb);
                Object remoteValue = field.get(fromDto);
                field.set(merged, (remoteValue != null) ? remoteValue : localValue);
            }
        }
        return (T) merged;
    }


}
