package com.ricardo.common.mapper;

import java.util.List;

public interface EntityMapper<D, E> {

    E mapTo(D dto);

    D mapFrom(E entity);

    List<E> mapListTo(List<D> dtoList);

    List<D> mapListFrom(List<E> entityList);
}
