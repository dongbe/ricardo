package com.ricardo.common.mapper;

import com.ricardo.common.constant.Patterns;
import com.ricardo.common.utils.DateUtils;

import java.time.LocalDate;

public class LocalDateMapper {

    public static LocalDate mapTo(String date) {
        return DateUtils.convertStringToDate(date, Patterns.DATE_PATTERN_FR_FULL);
    }

    public static String mapFrom(LocalDate date) {
        return DateUtils.convertLocalDateToString(date, Patterns.DATE_PATTERN_FR_FULL);
    }
}