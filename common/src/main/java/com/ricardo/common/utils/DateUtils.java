package com.ricardo.common.utils;

import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;

public final class DateUtils {

    /**
     * get date ignoring time
     */
    public static Date getRefDate(Date fullDate) {
        Date result = set(fullDate, Calendar.HOUR_OF_DAY, 0);
        result = set(result, Calendar.MINUTE, 0);
        result = set(result, Calendar.SECOND, 0);
        result = set(result, Calendar.MILLISECOND, 0);
        return result;
    }

    public static Date getRefTomorrowDate(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DAY_OF_YEAR, 1);
        return getRefDate(cal.getTime());
    }

    private static Date set(Date date, int calendarField, int amount) {
        Calendar c = Calendar.getInstance();
        c.setLenient(false);
        c.setTime(date);
        c.set(calendarField, amount);
        return c.getTime();
    }

    public static boolean isSameDay(Date date1, Date date2) {
        if (date1 == null || date2 == null) {
            throw new IllegalArgumentException("The date must not be null");
        }
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);
        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);
        return isSameDay(cal1, cal2);
    }

    public static boolean isSameDay(Calendar cal1, Calendar cal2) {
        if (cal1 == null || cal2 == null) {
            throw new IllegalArgumentException("The date must not be null");
        }
        return (cal1.get(Calendar.ERA) == cal2.get(Calendar.ERA) && cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR)
                && cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR));
    }

    public static Boolean isBetween(Date reference, Date start, Date end) {
        return isAfterInclusive(removeTime(reference), removeTime(start))
                && isBeforeInclusive(removeTime(reference), removeTime(end));
    }

    public static Boolean isBeforeInclusive(Date reference, Date end) {
        return end == null || reference.compareTo(end) <= 0;
    }

    public static Boolean isAfterInclusive(Date reference, Date start) {
        return start == null || reference.compareTo(start) >= 0;
    }

    public static Boolean isBefore(Date reference, Date end) {
        return reference.compareTo(end) < 0;
    }

    public static Boolean isAfter(Date reference, Date start) {
        return reference.compareTo(start) > 0;
    }

    public static Boolean isDayBefore(Date reference, Date end) {
        reference = removeTime(reference);
        end = removeTime(end);
        return end == null || (reference != null && reference.compareTo(end) < 0);
    }

    public static Boolean isDayAfter(Date reference, Date start) {
        reference = removeTime(reference);
        start = removeTime(start);
        return start == null || (reference != null && reference.compareTo(start) > 0);
    }

    public static Date removeTime(Date date) {
        if (date != null) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            cal.set(Calendar.HOUR_OF_DAY, 0);
            cal.set(Calendar.MINUTE, 0);
            cal.set(Calendar.SECOND, 0);
            cal.set(Calendar.MILLISECOND, 0);
            return cal.getTime();
        }
        return null;
    }

    public static DayOfWeek getJourDeLaSemaine(Date reference) {
        LocalDate date = reference.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        return date.getDayOfWeek();
    }

    public static Boolean isSamedi(Date reference) {
        return getJourDeLaSemaine(reference).equals(DayOfWeek.SATURDAY);
    }

    public static Boolean isDimanche(Date reference) {
        return getJourDeLaSemaine(reference).equals(DayOfWeek.SUNDAY);
    }

    public static Date convertToDate(String s) {
        LocalDate localDate = LocalDate.parse(s);
        return Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
    }

    public static Date convertToDateTime(String s) {

        return Date.from(LocalDateTime.parse(s).atZone(ZoneId.systemDefault()).toInstant());
    }

    public static LocalDate convertStringToDate(String date, String format) {
        if (date == null) {
            return null;
        }
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
        return LocalDate.parse(date, formatter);
    }

    public static String convertDateToString(Date date) {
        if (date == null) {
            return "";
        }
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmm");
        return formatter.format(date);
    }

    public static String convertLocalDateToString(LocalDate localDate, String format) {
        if (localDate == null) {
            return "";
        }
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
        return localDate.format(formatter);
    }

    public static LocalDateTime getLocalDateTime(Date date) {
        return getLocalDateTime(date, "yyyyMMddHHmm");
    }

    public static LocalDateTime getLocalDateTime(Date date, String patternDate) {
        DateTimeFormatter customFormatter = DateTimeFormatter.ofPattern(patternDate);
        return LocalDateTime.parse(convertDateToString(date), customFormatter);
    }

    public static LocalDate convertToLocalDate(Date dateToConvert) {
        return dateToConvert.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    }

    public static String convertInstantToString(Instant instant, String patternDate) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(patternDate);

        return LocalDateTime.ofInstant(instant, ZoneId.systemDefault()).format(formatter);
    }

    public static Instant convertStringToInstant(String instantStr, String patternDate) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(patternDate);
        return LocalDateTime.parse(instantStr, formatter)
                .atZone(ZoneId.systemDefault()).toInstant();
    }

    public static LocalTime convertStringToLocalTime(String date) {
        if (date == null) {
            return null;
        }
        return LocalTime.parse(date);
    }

    public static String convertLocalTimeToString(LocalTime date) {
        if (date == null) {
            return "";
        }
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("H:mm");
        return date.format(formatter);
    }
}
